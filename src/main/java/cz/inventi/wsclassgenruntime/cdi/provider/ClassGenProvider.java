package cz.inventi.wsclassgenruntime.cdi.provider;

import java.lang.reflect.ParameterizedType;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Named;

import cz.inventi.wsclassgenruntime.generator.ClassImplGenerator;
import cz.inventi.wsclassgenruntime.cdi.Magic;

/**
 *
 * @author Tomas Poledny {@literal <tomas.poledny at inventi.cz>}
 */
@Named
public class ClassGenProvider {

	@Produces
	@Magic
	@SuppressWarnings("unchecked")
	public <T> Wrapper<T> produceMagic(InjectionPoint injectionPoint) {
		ParameterizedType parameterizedType = (ParameterizedType) injectionPoint.getType();
		Class<T> clazz = (Class<T>) parameterizedType.getActualTypeArguments()[0];
		T instance = ClassImplGenerator.createObject(clazz);
		return new Wrapper<T>(instance);

	}

}
