package cz.inventi.wsclassgenruntime.cdi.provider;

/**
 *
 * @author Tomas Poledny {@literal <tomas.poledny at inventi.cz>}
 */
public class Wrapper<T> {
	private final T bean;

	public Wrapper(T bean) {
		this.bean = bean;
	}

	public T getBean() {
		return bean;
	}
	
}
