package cz.inventi.wsclassgenruntime.generator;

/**
 * Generator proxy
 *
 * @author Tomas Poledny {@literal <tomas.poledny at inventi.cz>}
 */
public class ClassImplGenerator {

    private ClassImplGenerator() {
    }

    @SuppressWarnings("unchecked")
    public static <T> T createObject(Class<T> clazz) {
        return null;
    }

}
