package cz.inventi.wsclassgenruntime.magic;

/**
 *
 * @author Tomas Poledny {@literal <tomas.poledny at inventi.cz>}
 */
public interface SuperClass {

    int computePlus(int a, int b);

}
