package cz.inventi.wsclassgenruntime.cdi.provider;

import javax.inject.Inject;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.runner.RunWith;

import cz.inventi.wsclassgenruntime.cdi.test.WeldJUnit4Runner;
import cz.inventi.wsclassgenruntime.generator.ClassImplGenerator;
import cz.inventi.wsclassgenruntime.cdi.Magic;
import cz.inventi.wsclassgenruntime.magic.SuperClass;

/**
 *
 * @author Tomas Poledny {@literal <tomas.poledny at inventi.cz>}
 */
@RunWith(WeldJUnit4Runner.class)
public class ClassGenProviderTest {

    @Inject
    @Magic
    private Wrapper<SuperClass> injectedClass;

    public ClassGenProviderTest() {
    }

    @Test
    public void testCDISuperClass() {
        SuperClass superClass = injectedClass.getBean();
        testSuperClassMethods(superClass);
    }

    @Test
    public void testSuperClass() {
        SuperClass superClass = ClassImplGenerator.createObject(SuperClass.class);
        testSuperClassMethods(superClass);
    }

    private void testSuperClassMethods(SuperClass superClass) {
        int result = superClass.computePlus(5, 6);
        assertEquals(11, result);
    }

}
